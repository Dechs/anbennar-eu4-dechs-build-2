government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = rabhidarubsad
religion = high_philosophy
technology_group = tech_raheni
religious_school = orange_sash_school
capital = 4426

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Vijaysenar"
		dynasty = "of the Savage Jolt"
		birth_date = 1429.1.2
		adm = 1
		dip = 2
		mil = 6
		culture = royal_harimari
	}
}