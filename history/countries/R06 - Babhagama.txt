government = monarchy
add_government_reform = great_jamindar_reform
government_rank = 1
primary_culture = royal_harimari
add_accepted_culture = sobhagand
religion = high_philosophy
technology_group = tech_harimari
religious_school = unbroken_claw_school
capital = 4450

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Mahipalar"
		dynasty = "of the Flower Oath"
		birth_date = 1398.2.6
		adm = 6
		dip = 3
		mil = 5
		culture = royal_harimari
	}
}