government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = rasarhid
religion = high_philosophy
technology_group = tech_raheni
religious_school = silk_turban_school
capital = 4504

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Ayusar"
		dynasty = "of the Heavenly Lotus"
		birth_date = 1410.3.25
		adm = 5
		dip = 4
		mil = 2
		culture = royal_harimari
	}
}