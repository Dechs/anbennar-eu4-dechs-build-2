government = republic
add_government_reform = oligarchy_reform
government_rank = 1
primary_culture = bluefoot_halfling
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 137

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1440.2.2 = {
	monarch = {
		name = "Willam of Cowskeep"
		birth_date = 1399.12.12
		adm = 3
		dip = 1
		mil = 1
	}
}