# Triggered modifiers are here.
# these are checked for each countries once/month and then applied.
#
# Effects are fully scriptable here.


# ###########################################
# # Monstrous Nation for monsterkind
# ###########################################
monstrous_nation = {
	potential = { #Cleans up the triggered modifier list, so it isn't filled with irrelevant modifiers.
		monstrous_culture = yes
	}

	trigger = {
			monstrous_culture = yes
			NOT = 
			{
				is_year = 1650
				has_country_modifier = jaddari_harpy_march
			}
	}
	diplomatic_reputation = -1
	unjustified_demands = -0.5
	hostile_attrition = 1
}

# ###########################################
# # Giantkind
# ###########################################
# giantkind_administration = {
	# potential = {
		# culture_group = giantkind
	# }
	
	# trigger = {
		# OR =
			# {
				# #culture_group = giantkind
			# }
			# NOT = {
				# #something to do "after this year"
			# }
		# }
	# global_colonial_growth = -10
	# land_morale = 0.10
	# hostile_attrition = 1.0
	# manpower_recovery_speed = -0.10
	# infantry_power = 0.10
# }


# ###########################################
# # Troll
# ###########################################
# troll_administration = {
	# potential = {
		# culture_is_troll = yes
	# }
	
	# trigger = {
		# OR =
			# {
				# culture_is_troll = yes
			# }
			# NOT = {
				# #something to do "after this year"
			# }
		# }
	# global_colonial_growth = -10
	# land_morale = 0.10
	# hostile_attrition = 1.0
	# manpower_recovery_speed = -0.10
	# infantry_power = 0.10
# }

# ###########################################
# # Gnollish
# ###########################################
# gnollish_administration = {
	# potential = {
		# culture_group = gnollish
	# }

	# trigger = {
			# OR = 
			# {
				# culture_group = gnollish
			# }
			# NOT = 
			# {
				# #something to do "after this year"
			# }
	# }
	# #
# }

# ###########################################
# # Goblin
# ###########################################
# goblin_administration = {
	# potential = {
		# culture_group = goblin
	# }

	# trigger = {
			# OR = 
			# {
				# culture_group = goblin
			# }
			# NOT = 
			# {
				# #something to do "after this year"
			# }
	# }
	# manpower_recovery_speed = 0.20
	# land_morale = -0.05
	# discipline = -0.1
	# land_forcelimit_modifier = 0.10
	# mil_tech_cost_modifier = -0.1
# }

# ###########################################
# # Harpy
# ###########################################
# harpy_administration = {
	# potential = {
		# culture_group = harpy
	# }

	# trigger = {
			# OR = 
			# {
				# culture_group = harpy
			# }
			# NOT = 
			# {
				# #something to do "after this year"
			# }
	# }
	# hostile_attrition = 1.5
	# female_advisor_chance = 1.5
	# land_attrition = -0.1
	# shock_damage_received = -0.15
	# fire_damage_received = 0.05
	# #manpower_recovery_speed = -0.25 TODO: Once The Hunt relgion mechanics are properly implemented, uncomment this.
	# movement_speed = 0.05
# }

# ###########################################
# # Kobold
# ###########################################
# kobold_administration = {
	# potential = {
		# culture_group = kobold
	# }

	# trigger = {
			# OR = 
			# {
				# culture_group = kobold
			# }
			# NOT = 
			# {
				# #something to do "after this year"
			# }
	# }
	# global_regiment_cost = -0.10
	# land_forcelimit_modifier = 0.15
	# manpower_recovery_speed = 0.25
	# land_morale = -0.1
	# discipline = -0.1
	# hostile_attrition = 1
# }

# ###########################################
# # Orcish
# ###########################################
# orcish_administration = {
	# potential = {
		# culture_group = orcish
	# }

	# trigger = {
			# OR = 
			# {
				# culture_group = orcish
			# }
			# NOT = 
			# {
				# #something to do "after this year"
			# }
	# }
	# land_morale = 0.15
	# discipline = -0.1
	# infantry_power = 0.25
	# war_exhaustion = -0.1
# }

# half_orcish_administration = {	#we dont have one for half elf tho
	# potential = {
		# culture_is_half_orcish  = yes
	# }

	# trigger = {
			# culture_is_half_orcish  = yes
	# }
	# land_morale = 0.15
	# discipline = -0.1
	# infantry_power = 0.25
	# war_exhaustion = -0.1
# }

# half_elven_administration = {	#we dont have one for half elf tho
	# potential = {
		# culture_is_half_elven  = yes
	# }

	# trigger = {
			# culture_is_half_elven  = yes
	# }

# }

# ruinborn_administration = {
	# potential = {
		# culture_group_is_ruinborn = yes
	# }

	# trigger = {
		# culture_group_is_ruinborn = yes
	# }

# }


###########################################
# Province ownership modifiers
###########################################
owns_anbenncost = {
	potential = {
		#normal_or_historical_administrations = yes
	}

	trigger = {
		owns = 8	#Anbenncost
	}
	diplomatic_upkeep = 1	
}

###########################################
# Empire of Anbennar dominant religion bonus
###########################################
hre_dominant_regent_court = {
	potential = {
		capital_scope = {
			continent = europe
		}
		OR = {
			religion = regent_court
			religion = corinite
		}
	}

	trigger = {
		religion = regent_court
		hre_religion = regent_court
		hre_religion_locked = yes
	}
	
	legitimacy = 0.25
	tolerance_own = 1
	global_missionary_strength = 0.01
	imperial_authority = 0.25
}

hre_dominant_corinite = {
	potential = {
		capital_scope = {
			continent = europe
		}
		OR = {
			religion = regent_court
			religion = corinite
		}
	}

	trigger = {
		religion = corinite
		hre_religion = corinite
		hre_religion_locked = yes
	}
	
	legitimacy = 0.25
	tolerance_own = 1
	global_missionary_strength = 0.01
	imperial_authority = 0.25
}
###########################################
# Reclaiming Dwarovar
###########################################
reclaiming_dwarovar = {
	potential = {
		#NOT = { is_year = 1600 }
		capital_scope = {
			OR = {
				# continent = europe
				# continent = africa
				continent = serpentspine
			}
		}
	}
	trigger = {
		#NOT = { is_year = 1600 }
		any_owned_province = {
			OR = {
				superregion = west_serpentspine_superregion
				superregion = east_serpentspine_superregion
			}
		}
	}
	colonists = 1
	native_uprising_chance = 0.5
	native_assimilation = -1.5
	migration_cooldown = -0.2
}

###########################################
# Racial Mil Balance if you dont have the DLC
###########################################

#Cradle of Civ - Army Proff and Drill
hobgoblin_military_cradle = {
	potential = {
		NOT = { has_dlc = "Cradle of Civilization" }
		has_country_modifier = hobgoblin_military
	}
	trigger = {
		NOT = { has_dlc = "Cradle of Civilization" }
		has_country_modifier = hobgoblin_military
	}
	# land_morale = -0.15
	# recover_army_morale_speed = -0.45
	
	drill_gain_modifier = -1
	yearly_army_professionalism = -0.01
	
	land_morale = 0.05
	recover_army_morale_speed = 0.1
}

elven_military_cradle = {
	potential = {
		NOT = { has_dlc = "Cradle of Civilization" }
		has_country_modifier = elven_military
	}
	trigger = {
		NOT = { has_dlc = "Cradle of Civilization" }
		has_country_modifier = elven_military
	}
	drill_gain_modifier = -1
	
	recover_army_morale_speed = 0.1
}