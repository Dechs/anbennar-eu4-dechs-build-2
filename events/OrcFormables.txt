
namespace = orcform

#New Country Formed - default
country_event = {
	id = orcform.1
	title = orcform.1.t
	desc = orcform.1.d
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		#government_type = monstrous_horde
		#government_rank = 2
	}
	
	option = {		# Orcish Kingdom
		name = "orcform.1.a"
		ai_chance = { 
			factor = 60
			modifier = {
				factor = 10
				tag = Z51
			}
			modifier = {
				factor = 2
				tag = Z50
			}
			modifier = {
				factor = 2
				has_reform = martial_society_reform
			}
		}	
		if = {
			limit = { NOT = { government = monarchy } }
			change_government = monarchy
		}
		add_government_reform = orcish_kingdom_reform
	}
	option = {		# Noble (Warrior) Republic
		name = "orcform.1.b"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 2	#Having a poor ruler makes it likely they want an elective country
				OR = {
					NOT = { adm = 3 }
					NOT = { dip = 3 }
					NOT = { mil = 3 }
				}
			}
			modifier = {
				factor = 4
				tag = Z53	# Barumand
			}
			modifier = {
				factor = 2
				OR = {
					tag = Z52	# Unguldavor
					tag = Z50	# Grombar
				}
			}
			modifier = {
				factor = 2
				has_reform = martial_society_reform
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = republic
		}
		add_government_reform = noble_elite_reform
	}
	option = {		# Orcish Theocracy
		name = "orcform.1.c"
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 8
				tag = Z53	# Unguldavor
			}
			modifier = {
				factor = 2
				tag = Z53	# Barumand
			}
			modifier = {
				factor = 2
				has_reform = religious_societies_reform
			}
		}
		if = {
			limit = { NOT = { government = theocracy } }
			change_government = theocracy
		}
		add_government_reform = leading_clergy_reform
	}
}

# Culture ties weakened
country_event = {
	id = orcform.2
	title = orcform.2.t
	desc = orcform.2.d
	picture = EUROPEAN_REFUGEES_eventPicture
	
	trigger = {
		has_country_flag = orc_nation_formed
		NOT = { has_country_flag = new_orc_culture }
		NOT = { has_country_modifier = legacy_of_greentide_invaders_modifier }
		
		num_of_cities = 5
		
		is_at_war = no
		is_vassal = no
	}
	
	mean_time_to_happen = {
		months = 200
		modifier = {
			factor = 0.7
			stability = 1
		}
		modifier = {
			factor = 0.25
			num_of_cities = 10
		}
		modifier = {
			factor = 0.1
			in_golden_age = yes
		}
	}
	
	option = { # Grombar
		name = orcform.2.a
		trigger = {
			tag = Z50
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = grombar_orc
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = grombar_orc
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = grombar_orc
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = grombar_orc
		}
		
		set_country_flag = new_orc_culture
		
		change_primary_culture = grombar_orc
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Khozrugan
		name = orcform.2.a
		trigger = {
			tag = Z51
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = rugan_orc
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = rugan_orc
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = rugan_orc
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = rugan_orc
		}
		
		set_country_flag = new_orc_culture
		
		change_primary_culture = rugan_orc
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Unguldavor
		name = orcform.2.a
		trigger = {
			tag = Z52
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = ungulan_orc
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = ungulan_orc
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = ungulan_orc
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = ungulan_orc
		}
		
		set_country_flag = new_orc_culture
		
		change_primary_culture = ungulan_orc
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
	
	option = { # Barumand
		name = orcform.2.a
		trigger = {
			tag = Z53
		}
		
		hidden_effect = {
			every_core_province = { 
				limit = {
					culture_group = ROOT
				}
				change_culture = barumand_orc
			}
		}
		
		if = {
			limit = { ruler_culture = ROOT }
			set_ruler_culture = barumand_orc
		}
		if = {
			limit = { has_heir = yes heir_culture = ROOT }
			set_heir_culture = barumand_orc
		}
		if = {
			limit = { has_consort = yes consort_culture = ROOT }
			set_consort_culture = barumand_orc
		}
		
		set_country_flag = new_orc_culture
		
		change_primary_culture = barumand_orc
		
		add_country_modifier = {
			name = birth_of_a_new_people 
			duration = 3650
		}
	}
}
