namespace = great_dookan_events

#Invoke warrior ancestors
country_event = {
	id = great_dookan_events.1
	title = great_dookan_events.1.t
	desc = great_dookan_events.1.d
	picture = BATTLE_eventPicture
	
	is_triggered_only = yes
	major = no
	
	option = {
		name = great_dookan_events.1.a
		
		add_country_modifier = {
			name = "dookan_reconnected_with_past_lives"
			duration = 75
		}
	}
}

#Purge the weak
country_event = {
	id = great_dookan_events.2
	title = great_dookan_events.2.t
	desc = great_dookan_events.2.d
	picture = FAMINE_eventPicture
	
	is_triggered_only = yes
	major = no
	
	option = {
		name = great_dookan_events.2.a
		
		random_owned_province = {
			limit = {
				OR = {
					has_owner_religion = no
					has_owner_religion = no
				}
			}
			add_devastation = 30
			change_culture = ROOT
			change_religion = ROOT
		}
	}
}

#Gather a great host
country_event = {
	id = great_dookan_events.3
	title = great_dookan_events.3.t
	desc = great_dookan_events.3.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	major = no
	
	option = {
		name = great_dookan_events.3.a
		
		add_yearly_manpower = 10
		add_country_modifier = {
			name = dookan_gather_great_host
			duration = 365
		}
	}
}

#Glorious looting
country_event = {
	id = great_dookan_events.4
	title = great_dookan_events.4.t
	desc = great_dookan_events.4.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	major = no
	
	option = {
		name = great_dookan_events.4.a
		
		random_province = {
			limit = {
				controlled_by = ROOT
				NOT = { owned_by = ROOT}
			}
			random_list = {
				1 = { add_base_tax = -1 }
				1 = { add_base_production = -1 }
				1 = { add_base_manpower = -1 }
			}
			random_list = {
				1 = { add_base_tax = -1 }
				1 = { add_base_production = -1 }
				1 = { add_base_manpower = -1 }
			}
			add_devastation = 50
		}
		
		capital_scope = {
			random_list = {
				1 = { add_base_tax = 1 }
				1 = { add_base_production = 1 }
				1 = { add_base_manpower = 1 }
			}
		}
	}
}

#anoint slave herd
country_event = {
	id = great_dookan_events.5
	title = great_dookan_events.5.t
	desc = great_dookan_events.5.d
	picture = FAMINE_eventPicture
	
	is_triggered_only = yes
	major = no
	
	option = {
		name = great_dookan_events.5.a
		
		random_owned_province = {
			limit = { trade_goods = slaves }
			random_list = {
				30 = { add_base_tax = 1 }
				40 = { add_base_production = 1 }
				30 = { add_base_manpower = 1 }
			}
			add_province_modifier = {
				name = "dookan_anointed_slave_herd"
				duration = 365
			}
		}
	}
}